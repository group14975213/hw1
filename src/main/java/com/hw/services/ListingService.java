package com.hw.services;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.hw.entities.Listing;
import com.hw.repository.ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
public class ListingService {

    @Autowired
    private ListingRepository listingRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    @PostConstruct
    public void init() throws Exception {

        //load csv data TODO see if need to make paged/batched
        Resource resource = resourceLoader.getResource("classpath:data/csv/listing-details.csv");//TODO move to properties
        CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        MappingIterator<Listing> readValues = mapper.readerFor(Listing.class).with(bootstrapSchema).readValues(resource.getFile());
        List<Listing> listings = readValues.readAll();
        listingRepository.saveAll(listings);

    }
    public Page<Listing> fetchListingsDataAsPageWithFiltering(
            double minMinCpm,
            double maxMinCpm,
            double minPrice,
            double maxMaxPrice,
            int page,
            int size) {

        Pageable pageable = PageRequest.of(page, size);
        return listingRepository.findByMinCpmBetweenAndPriceBetween(minMinCpm, maxMinCpm, minPrice, maxMaxPrice, pageable);
    }

}
