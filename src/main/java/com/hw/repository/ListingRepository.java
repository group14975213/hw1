package com.hw.repository;

import com.hw.entities.Listing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingRepository extends JpaRepository<Listing, Long> {

    Page<Listing> findByMinCpmBetweenAndPriceBetween(double minMinCpm, double maxMinCpm, Double minPrice, Double maxMaxPrice, Pageable pageable);
}