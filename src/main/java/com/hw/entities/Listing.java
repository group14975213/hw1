package com.hw.entities;

import javax.persistence.*;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="listings", indexes = { @Index(name = "minmax_mincpm_price_ndx", columnList = "minCpm, price") })
public class Listing {
    @Id
    private String sessionId;
    @Column(nullable = false)
    private String advertiserId;
    @Column(nullable = false, length = 2) //TODO use enum
    private String country;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Double priority;
    @Column(nullable = false)
    private Double bidPrice;
    @Column(nullable = false)
    private Integer eventTypeId;//TODO make enum
    @Column(nullable = false)
    private Integer gdpr;//TODO make enum
    @Column(nullable = false)
    private Double minCpm;

}