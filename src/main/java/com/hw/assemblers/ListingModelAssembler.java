package com.hw.assemblers;

import com.hw.controllers.ListingController;
import com.hw.entities.Listing;
import com.hw.model.ListingModel;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class ListingModelAssembler extends RepresentationModelAssemblerSupport<Listing, ListingModel> {
    public ListingModelAssembler() {
        super(ListingController.class, ListingModel.class);
    }

    @Override
    public ListingModel toModel(Listing entity) {
        ListingModel model = new ListingModel();
        BeanUtils.copyProperties(entity, model);
        return model;
    }
}
