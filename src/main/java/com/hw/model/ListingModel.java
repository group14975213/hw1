package com.hw.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListingModel extends RepresentationModel<ListingModel> {

    private String advertiserId;
    private String country;
    private Double price;
    private Double priority;
    private Double bidPrice;
    private Integer eventTypeId;//TODO make enum
    private Integer gdpr;//TODO make enum
    private Double minCpm;

}