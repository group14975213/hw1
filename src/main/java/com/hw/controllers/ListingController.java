package com.hw.controllers;

import com.hw.assemblers.ListingModelAssembler;
import com.hw.entities.Listing;
import com.hw.model.ListingModel;
import com.hw.services.ListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController()
@RequestMapping("listings")
public class ListingController {

        @Autowired
        private ListingService listingService;

        @Autowired
        private ListingModelAssembler listingModelAssembler;

        @Autowired
        private PagedResourcesAssembler<Listing> pagedResourcesAssembler;

        @GetMapping
        public PagedModel<ListingModel> fetchListingsWithPagination(//TODO add sort (field, direction)
                @RequestParam(name="min_min_cpm", defaultValue = "0.0") double minMinCpm,
                @RequestParam(name="max_min_cpm", defaultValue = "" + Double.MAX_VALUE) double maxMinCpm,
                @RequestParam(name="min_price", defaultValue = "" + Double.MIN_VALUE) double minPrice,
                @RequestParam(name="max_price", defaultValue = "" + Double.MAX_VALUE) double maxPrice,
                @RequestParam(defaultValue = "0") int page,
                @RequestParam(defaultValue = "30") int size) {

            Page<Listing> listingsPage = listingService.fetchListingsDataAsPageWithFiltering(minMinCpm, maxMinCpm, minPrice, maxPrice, page, size);
            return pagedResourcesAssembler.toModel(listingsPage, listingModelAssembler);
        }
    }