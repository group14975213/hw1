FROM openjdk:8-jdk-alpine
MAINTAINER hw
COPY target/hw.jar hw.jar
ENTRYPOINT ["java","-jar","/hw.jar"]